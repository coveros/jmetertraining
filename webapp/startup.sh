#! /bin/bash

nginx
mysqld --user=root --initialize-insecure
mysqld --user=root &
sleep 15s
echo "CREATE DATABASE training" | mysql -uroot

echo "CREATE TABLE User (id int(11) unsigned NOT NULL AUTO_INCREMENT,username varchar(20) NOT NULL DEFAULT '',password varchar(20) DEFAULT NULL,PRIMARY KEY (id));" | mysql -uroot training
echo "CREATE TABLE Course (id int(11) unsigned NOT NULL AUTO_INCREMENT,title varchar(100) NOT NULL DEFAULT '',description varchar(256) DEFAULT NULL,duration varchar(100) DEFAULT NULL,PRIMARY KEY (id));" | mysql -uroot training

echo "INSERT INTO User (id, username, password) values (1,'tester1','tester1');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (2,'tester2','tester2');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (3,'tester3','tester3');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (4,'tester4','tester4');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (5,'tester5','tester5');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (6,'tester6','tester6');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (7,'tester7','tester7');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (8,'tester8','tester8');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (9,'tester9','tester9');" | mysql -uroot training
echo "INSERT INTO User (id, username, password) values (10,'tester10','tester10');" | mysql -uroot training

./usr/share/tomcat/bin/catalina.sh run