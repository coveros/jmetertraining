# JMeter Course Material
All material in this repository is for the JMeter Training Course

## Overview

Day 1:

# Setup Environment
Set up our load testing exercise environment and complete a simple exercise. 

# Intro to jmeter
What is jmeter? What are the limitations of jmeter? How you should develop jmeter. How you should run jmeter at scale. 

# How to record a test
Common tools you should use to help capture web traffic (zap proxy, wireshark) How to set up the tools. Jmeter script recorder.

# Dealing with SSL
SSL basics, trusts, generating certificates with openssl. Running jmeter tests with https sights. 

# How to build a test from a record script
What to and not to parameterize. How to generalize code from the test recorder. 

Day 2:

# Running tests
How to orchestrate tests from jenkins and the blazemeter plugin. 

# Picking test scenarios
How to use manual or automated tests to pick the best load tests to perform. 

# Running test in parallel
How to do dependency analysis between tests. What you need to know from the application you are testing.  

# What to monitor during the test
What to monitor in the tests themselves. What to measure on the target servers. What to monitor in the application stack(apache, mysql). What monitoring to use and not to use (Some monitoring can bog down the system) Disk space concerns. 

# Results aggregation and reporting
Free tools for remote log collection. How to automate log collection. What bottlenecks can look look. Common pitfalls of tuning tomcat and java applications.  Introduction to jenkins dashboarding. 

# Scaling your tests in the cloud
Reading test cases to plan for tests. Designing test cases that set up and tear down on their own. When to create scripts to populate data. Ensuring system start and end state (Tear down threads)


## Training WebApp

The webapp is a quick and dirty webapp developed for use in this class. The class exercises are built around this webapp. 

To run the webapp:

1. sudo docker build -t training
2. sudo docker run -p 8081:8080 -p 9443:443 -it training
3. Default username/password: tester1/tester1


## Jenkins Requirements
Exercises 6 and 7 require a couple packages (bzt and virtualenv) to be installed on the server running Jenkins. Execute install-packages.sh to install these packages.